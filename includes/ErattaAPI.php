<?php
function getErattaJsonDate($param)
{
	$erattaEndpoint = 'https://access.redhat.com/hydra/rest/securitydata/cvrf.json'.$param;
	$option = [
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_TIMEOUT => 10,
	];
	
	$ch = curl_init($erattaEndpoint);
	curl_setopt_array($ch, $option);

	$json = curl_exec($ch);
	$info = curl_getinfo($ch);
	$errorNo = curl_errno($ch);
	if($errorNo !== CURLE_OK or $info['http_code'] !== 200)
	{
		return '[ERR!]'.$errorNo;
	}

	return $json;
}

function getRHSAJsonDate($RHSA_ID)
{
	$erattaEndpoint = 'https://access.redhat.com/hydra/rest/securitydata/cvrf/'. $RHSA_ID .'.json';
	$option = [
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_TIMEOUT => 10,
	];
	
	$ch = curl_init($erattaEndpoint);
	curl_setopt_array($ch, $option);

	$json = curl_exec($ch);
	$info = curl_getinfo($ch);
	$errorNo = curl_errno($ch);
	if($errorNo !== CURLE_OK or $info['http_code'] !== 200)
	{
		return '[ERR!]'.$errorNo;
	}
	return $json;
}
?>