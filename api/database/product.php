<?php
require($_SERVER['DOCUMENT_ROOT'].'/includes/DatabaseUtils.php');

print_r(json_encode(getProductArray()));

function getProductArray(){
    $array_result = array();
    $sql = "select * from RHSA_PRODUCT";
    $mysqli = getMySQLi();

    if($mysqli->connect_error)
    {
        echo $mysqli->connect_error;
        exit();
    }
    $mysqli->set_charset("utf8");

    if($result = $mysqli->query($sql))
    {
        while($row = $result->fetch_assoc())
        {
            $array_result[] = $row;
        }
    }
    $result->close();
    $mysqli->close();
    return $array_result;
}

?>