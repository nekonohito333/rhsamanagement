<?php 

require($_SERVER['DOCUMENT_ROOT'].'/includes/ErattaAPI.php');

$erattaParam = $_GET['per_page'] == '' ? '?per_page=10' : '?per_page='.$_GET['per_page'];
$erattaParam .= $_GET['before'] == '' ? '' : '&before='.$_GET['before'];
$erattaParam .= $_GET['after'] == '' ? '' : '&after='.$_GET['after'];
$erattaParam .= $_GET['rhsa_ids'] == '' ? '' : '&rhsa_ids='.$_GET['rhsa_ids'];
$erattaParam .= $_GET['cve'] == '' ? '' : '&cve='.$_GET['cve'];
$erattaParam .= $_GET['severity'] == '' ? '' : '&severity='.$_GET['severity'];
$erattaParam .= $_GET['page'] == '' ? '' : '&page='.$_GET['page'];
$erattaParam .= $_GET['created_days_ago'] == '' ? '' : '&created_days_ago='.$_GET['created_days_ago'];

print(getErattaJsonDate($erattaParam));

?>
