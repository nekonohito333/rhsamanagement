

function initSidebar(){
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
        $('.content').toggleClass('active');
    });
}

function sendModalCommon( title, msg){
    $('#modal-common-title').text(title);
    $('#modal-common-body').text(msg);
    $('#modal-common').modal({
        backdrop: 'static'
    });
    $('#modal-common').modal('show');
}
function sendModalCommonFromHtml( title, html){
    $('#modal-common-title').text(title);
    $('#modal-common-body').html(html);
    $('#modal-common').modal({
        backdrop: 'static'
    });
    $('#modal-common').modal('show');
}

function sendToastCommon( title, msg)
{
    $('#toast-common-title').text(title);
    $('#toast-common-message').text(msg);
    $('#toast-common').toast('show');
}
function sendToastCommonFromHtml( title, html)
{
    $('#toast-common-title').text(title);
    $('#toast-common-message').html(html);
    $('#toast-common').toast('show');
}