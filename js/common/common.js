/**
 * Common Area
 * 
 */

// Page Load Complete EventTrigger
$(document).ready(function()
{
    if(!isSupportedBrowser()){
        sendModalCommon('Warning','InternetExploer is Not Supportted');
        return;
    }
    //sendToastCommonFromHtml( "統計情報", "今週のセキュリティ収集件数 : なし<br>緊急性の高いアラート : なし");
    initSidebar();
    reflashData();
});


/**
 * Sidebar Area
 * 
 */

// Sidebar AboutBtn EventTrigger
$('#sidebar-about').on('click',function(){
    sendModalCommonFromHtml('About','<h4>Product</h4><p>RHSAManagement ver 1.0</p><h4>Framework</h4><p>SKBootstrapModule ver 1.0<br>Bootstrap ver 4.3.1<br>jQuery ver 3.3.1</p>');
});


/**
 * Toolbar Area
 */

// Reflash Button EventTrigger
$('#toolbar-reflash').on('click',function(){
    sendModalCommonFromHtml('Confirmation','データベース更新中はセキュリティデータの閲覧ができなくなります。<br>データベース更新を行いますか？');
});

// Reflash Button EventTrigger
$('#toolbar-delete').on('click',function(){
    sendModalCommonFromHtml('Confirmation','選択したセキュリティ情報を削除しますか？<br>（同期処理の関係上データベースからの削除は行われません）');
});



//
function isSupportedBrowser(){
    let userAgent = window.navigator.userAgent.toLowerCase();
    if(userAgent.indexOf('msie') != -1 || userAgent.indexOf('trident') != -1) return false;
    return true;
}