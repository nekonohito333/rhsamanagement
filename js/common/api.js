
const rhsa_endpoint = "/api/";

function reflashData()
{
    fetch(rhsa_endpoint + 'bypass.php?per_page=50').then(function(responce){
        return responce.json();
    }).then(function(json){
        setDashboardRHSATable(json);
    });
}


async function setDashboardRHSATable(json)
{
    json.forEach(rhsa => {
        console.log(rhsa);
        $('#table-rhsa-content').append(`<tr data-rhsa-id="${rhsa.RHSA}"><th class="${rhsa.severity}">${rhsa.RHSA}</th><td>null</td><td>${rhsa.severity}</td><td>null</td><td>${rhsa.released_on}</td></tr>`)
    });
    $('#loading-rhsa').html("");

    $('#table-rhsa-content tr').on('click',function(){
        //console.log($(this).data('rhsa-id'));
        $(this).toggleClass('row-select');
    });
}

